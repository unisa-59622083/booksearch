# BookSearch #

This README will give an outline to this repository, and explain how to set it up (if necessary).

### What is this repository for? ###

- This is my submission to a university assignment
- The requirements was to create a database application useful to the community

### How do I get set up? ###

You require:

- Qt Designer (for pyQt)
- pyuic4 for generating Python code from .ui files
- Python 3.4 at least

To set up:
- Simply run the build.bat file at the root of this repository.