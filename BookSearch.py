# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'BookSearch.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(962, 463)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.mdiArea = QtGui.QMdiArea(self.centralwidget)
        self.mdiArea.setGeometry(QtCore.QRect(0, 0, 961, 421))
        self.mdiArea.setObjectName(_fromUtf8("mdiArea"))
        self.subwindow_read = QtGui.QWidget()
        self.subwindow_read.setObjectName(_fromUtf8("subwindow_read"))
        self.gridLayoutWidget = QtGui.QWidget(self.subwindow_read)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 921, 361))
        self.gridLayoutWidget.setObjectName(_fromUtf8("gridLayoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.btn_search = QtGui.QPushButton(self.gridLayoutWidget)
        self.btn_search.setObjectName(_fromUtf8("btn_search"))
        self.gridLayout.addWidget(self.btn_search, 0, 3, 1, 1)
        self.tbl_summary = QtGui.QTableWidget(self.gridLayoutWidget)
        self.tbl_summary.setRowCount(0)
        self.tbl_summary.setColumnCount(4)
        self.tbl_summary.setObjectName(_fromUtf8("tbl_summary"))
        self.tbl_summary.horizontalHeader().setDefaultSectionSize(218)
        self.gridLayout.addWidget(self.tbl_summary, 2, 0, 1, 4)
        self.cb_area = QtGui.QComboBox(self.gridLayoutWidget)
        self.cb_area.setEditable(False)
        self.cb_area.setInsertPolicy(QtGui.QComboBox.InsertAtTop)
        self.cb_area.setObjectName(_fromUtf8("cb_area"))
        self.gridLayout.addWidget(self.cb_area, 0, 0, 1, 1)
        self.cb_store = QtGui.QComboBox(self.gridLayoutWidget)
        self.cb_store.setEditable(False)
        self.cb_store.setObjectName(_fromUtf8("cb_store"))
        self.gridLayout.addWidget(self.cb_store, 0, 1, 1, 1)
        self.cb_book = QtGui.QComboBox(self.gridLayoutWidget)
        self.cb_book.setEditable(False)
        self.cb_book.setInsertPolicy(QtGui.QComboBox.InsertAtTop)
        self.cb_book.setObjectName(_fromUtf8("cb_book"))
        self.gridLayout.addWidget(self.cb_book, 0, 2, 1, 1)
        self.subwindow_create = QtGui.QWidget()
        self.subwindow_create.setObjectName(_fromUtf8("subwindow_create"))
        self.layoutWidget_2 = QtGui.QWidget(self.subwindow_create)
        self.layoutWidget_2.setGeometry(QtCore.QRect(10, 10, 601, 93))
        self.layoutWidget_2.setObjectName(_fromUtf8("layoutWidget_2"))
        self.gridLayout_5 = QtGui.QGridLayout(self.layoutWidget_2)
        self.gridLayout_5.setObjectName(_fromUtf8("gridLayout_5"))
        self.lbl_create_store = QtGui.QLabel(self.layoutWidget_2)
        self.lbl_create_store.setObjectName(_fromUtf8("lbl_create_store"))
        self.gridLayout_5.addWidget(self.lbl_create_store, 2, 0, 1, 1)
        self.tb_create_store = QtGui.QLineEdit(self.layoutWidget_2)
        self.tb_create_store.setObjectName(_fromUtf8("tb_create_store"))
        self.gridLayout_5.addWidget(self.tb_create_store, 2, 1, 1, 1)
        self.lbl_create_area = QtGui.QLabel(self.layoutWidget_2)
        self.lbl_create_area.setObjectName(_fromUtf8("lbl_create_area"))
        self.gridLayout_5.addWidget(self.lbl_create_area, 0, 0, 1, 1)
        self.lbl_create_book = QtGui.QLabel(self.layoutWidget_2)
        self.lbl_create_book.setObjectName(_fromUtf8("lbl_create_book"))
        self.gridLayout_5.addWidget(self.lbl_create_book, 3, 0, 1, 1)
        self.tb_create_area = QtGui.QLineEdit(self.layoutWidget_2)
        self.tb_create_area.setObjectName(_fromUtf8("tb_create_area"))
        self.gridLayout_5.addWidget(self.tb_create_area, 0, 1, 1, 1)
        self.tb_create_book = QtGui.QLineEdit(self.layoutWidget_2)
        self.tb_create_book.setObjectName(_fromUtf8("tb_create_book"))
        self.gridLayout_5.addWidget(self.tb_create_book, 3, 1, 1, 1)
        self.btn_create = QtGui.QPushButton(self.subwindow_create)
        self.btn_create.setGeometry(QtCore.QRect(143, 100, 75, 23))
        self.btn_create.setObjectName(_fromUtf8("btn_create"))
        self.lbl_create = QtGui.QLabel(self.subwindow_create)
        self.lbl_create.setGeometry(QtCore.QRect(150, 140, 451, 16))
        self.lbl_create.setText(_fromUtf8(""))
        self.lbl_create.setObjectName(_fromUtf8("lbl_create"))
        self.subwindow_edit = QtGui.QWidget()
        self.subwindow_edit.setObjectName(_fromUtf8("subwindow_edit"))
        self.layoutWidget_3 = QtGui.QWidget(self.subwindow_edit)
        self.layoutWidget_3.setGeometry(QtCore.QRect(10, 10, 601, 93))
        self.layoutWidget_3.setObjectName(_fromUtf8("layoutWidget_3"))
        self.gridLayout_6 = QtGui.QGridLayout(self.layoutWidget_3)
        self.gridLayout_6.setObjectName(_fromUtf8("gridLayout_6"))
        self.lbl_edit_store = QtGui.QLabel(self.layoutWidget_3)
        self.lbl_edit_store.setObjectName(_fromUtf8("lbl_edit_store"))
        self.gridLayout_6.addWidget(self.lbl_edit_store, 2, 0, 1, 1)
        self.tb_edit_store = QtGui.QLineEdit(self.layoutWidget_3)
        self.tb_edit_store.setObjectName(_fromUtf8("tb_edit_store"))
        self.gridLayout_6.addWidget(self.tb_edit_store, 2, 1, 1, 1)
        self.lbl_edit_area = QtGui.QLabel(self.layoutWidget_3)
        self.lbl_edit_area.setObjectName(_fromUtf8("lbl_edit_area"))
        self.gridLayout_6.addWidget(self.lbl_edit_area, 0, 0, 1, 1)
        self.lbl_edit_book = QtGui.QLabel(self.layoutWidget_3)
        self.lbl_edit_book.setObjectName(_fromUtf8("lbl_edit_book"))
        self.gridLayout_6.addWidget(self.lbl_edit_book, 3, 0, 1, 1)
        self.tb_edit_area = QtGui.QLineEdit(self.layoutWidget_3)
        self.tb_edit_area.setObjectName(_fromUtf8("tb_edit_area"))
        self.gridLayout_6.addWidget(self.tb_edit_area, 0, 1, 1, 1)
        self.tb_edit_book = QtGui.QLineEdit(self.layoutWidget_3)
        self.tb_edit_book.setObjectName(_fromUtf8("tb_edit_book"))
        self.gridLayout_6.addWidget(self.tb_edit_book, 3, 1, 1, 1)
        self.btn_edit = QtGui.QPushButton(self.subwindow_edit)
        self.btn_edit.setGeometry(QtCore.QRect(143, 100, 75, 23))
        self.btn_edit.setObjectName(_fromUtf8("btn_edit"))
        self.lbl_edit = QtGui.QLabel(self.subwindow_edit)
        self.lbl_edit.setGeometry(QtCore.QRect(150, 130, 451, 16))
        self.lbl_edit.setText(_fromUtf8(""))
        self.lbl_edit.setObjectName(_fromUtf8("lbl_edit"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 962, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.subwindow_read.setWindowTitle(_translate("MainWindow", "Read", None))
        self.btn_search.setText(_translate("MainWindow", "Search", None))
        self.subwindow_create.setWindowTitle(_translate("MainWindow", "Create", None))
        self.lbl_create_store.setText(_translate("MainWindow", "Library / Bookstore", None))
        self.lbl_create_area.setText(_translate("MainWindow", "Area", None))
        self.lbl_create_book.setText(_translate("MainWindow", "Book title                         ", None))
        self.btn_create.setText(_translate("MainWindow", "Create", None))
        self.subwindow_edit.setWindowTitle(_translate("MainWindow", "Edit", None))
        self.lbl_edit_store.setText(_translate("MainWindow", "Library / Bookstore", None))
        self.lbl_edit_area.setText(_translate("MainWindow", "Area", None))
        self.lbl_edit_book.setText(_translate("MainWindow", "Book title                         ", None))
        self.btn_edit.setText(_translate("MainWindow", "Update", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

