class Book:
    id = -1
    title = ""

    def __init__(self, ident, title):
        self.id = ident
        self.title = title

class Area:
    id = -1
    name = ""

    def __init__(self, ident, name):
        self.id = ident
        self.name = name

class BookDealer:
    id = -1
    name = ""

    def __init__(self, ident, name):
        self.id = ident
        self.name = name

class Summary:
    area = None
    book_dealer = None
    book = None

    def __init__(self, area, book_dealer, book):
        self.area = area
        self.book_dealer = book_dealer
        self.book = book