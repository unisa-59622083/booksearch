CREATE DEFINER=`root`@`localhost` PROCEDURE `update_generic`(IN pAreaId INT(11), IN pAreaName VARCHAR(255), IN pBookDealerId INT(11), IN pBookDealerName VARCHAR(255), IN pBookId INT(11), IN pBookTitle VARCHAR(255))
BEGIN
	IF NOT ISNULL(pAreaId) THEN
		UPDATE areas SET `name` = pAreaName WHERE id = pAreaId;
	END IF;
    
    IF NOT ISNULL(pBookDealerId) THEN
		UPDATE book_dealers SET `name` = pBookDealerName WHERE id = pBookDealerId;
	END IF;
    
    IF NOT ISNULL(pBookId) THEN
		UPDATE books SET title = pBookTitle WHERE id = pBookId;
	END IF;
END