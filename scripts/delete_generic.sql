CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_generic`(IN pAreaId INT(11), IN pBookDealerId INT(11), IN pBookId INT(11))
BEGIN
	SELECT @bookDealerAreaId := id FROM book_dealer_areas WHERE area_id = pAreaId AND book_dealer_id = pBookDealerId;
    
	DELETE FROM book_dealer_area_books WHERE book_dealer_area_id = @bookDealerAreaId AND book_id = pBookId;
    DELETE FROM book_dealer_areas WHERE id = @bookDealerAreaId;
    
    IF NOT EXISTS (SELECT * FROM book_dealer_areas WHERE area_id = pAreaId LIMIT 1) THEN
		CALL delete_area(pAreaId);
	END IF;
    
    IF NOT EXISTS (SELECT * FROM book_dealer_areas WHERE book_dealer_id = pBookDealerId LIMIT 1) THEN
		CALL delete_book_dealer(pBookDealerId);
	END IF;
    
    IF NOT EXISTS (SELECT * FROM book_dealer_area_books WHERE book_id = pBookId LIMIT 1) THEN
		CALL delete_book(pBookId);
	END IF;
END