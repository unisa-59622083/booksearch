CREATE DEFINER=`root`@`localhost` PROCEDURE `get_summary`(IN pAreaId INT(11), IN pBookDealerId INT(11), IN pBookId INT(11))
BEGIN
	SELECT
		a.id as area_id,
		a.`name` as area_name,
        bd.id as book_dealer_id,
		bd.`name` as book_dealer_name,
        b.id as book_id,
		b.title as book_title
	FROM
		areas a
		INNER JOIN book_dealer_areas bda on a.id = bda.area_id
		INNER JOIN book_dealers bd on bda.book_dealer_id = bd.id
		INNER JOIN book_dealer_area_books bdab on bda.id = bdab.book_dealer_area_id
		INNER JOIN books b on bdab.book_id = b.id
	WHERE
		(a.id = pAreaId OR pAreaId = -1)
        AND (bd.id = pBookDealerId OR pBookDealerId = -1)
        AND (b.id = pBookId OR pBookId = -1)
	ORDER BY
		b.title;
END