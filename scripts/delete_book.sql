CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_book`(IN pBookId INT(11))
BEGIN
	DELETE FROM books WHERE id = pBookId;
END