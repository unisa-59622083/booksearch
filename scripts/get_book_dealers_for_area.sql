CREATE DEFINER=`root`@`localhost` PROCEDURE `get_book_dealers_for_area`(IN pAreaId INT(11))
BEGIN
	SELECT
		bd.*
	FROM
		book_dealers bd
        INNER JOIN book_dealer_areas bda on bd.id = bda.book_dealer_id
	WHERE
		bda.area_id = pAreaId
	ORDER BY
		`name`;
END