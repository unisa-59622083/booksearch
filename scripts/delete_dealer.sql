CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_dealer`(IN pBookDealerId INT(11))
BEGIN
	DELETE FROM book_dealers WHERE id = pBookDealerId;
END