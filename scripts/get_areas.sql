CREATE DEFINER=`root`@`localhost` PROCEDURE `get_areas`()
BEGIN
	SELECT * FROM areas ORDER BY `name`;
END