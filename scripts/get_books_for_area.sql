CREATE DEFINER=`root`@`localhost` PROCEDURE `get_books_for_area`(IN pAreaId INT(11))
BEGIN
	SELECT
		b.*
	FROM
		books b
		INNER JOIN book_dealer_area_books bdab on b.id = bdab.book_id
        INNER JOIN book_dealer_areas bda on bdab.book_dealer_area_id = bda.id
	WHERE
		bda.area_id = pAreaId
	GROUP BY
		b.id
	ORDER BY
		b.title;
END