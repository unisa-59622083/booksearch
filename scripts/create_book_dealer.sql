CREATE DEFINER=`root`@`localhost` PROCEDURE `create_book_dealer`(IN pBookDealerName VARCHAR(255), IN pAreaId INT(11), INOUT pBookDealerId INT(11), OUT pBookDealerAreaId INT(11))
BEGIN
    SELECT @bookDealerId := IFNULL((SELECT id FROM book_dealers WHERE `name` = pBookDealerName LIMIT 1), NULL);

	IF ISNULL(@bookDealerId) THEN
		INSERT INTO book_dealers (`name`) VALUES (pBookDealerName);
		SET @bookDealerId := LAST_INSERT_ID();
    END IF;
    
    SET pBookDealerId := @bookDealerId;
    
    IF NOT EXISTS (SELECT * FROM book_dealer_areas WHERE area_id = pAreaId AND book_dealer_id = @bookDealerId LIMIT 1) THEN
		INSERT INTO book_dealer_areas (book_dealer_id, area_id) VALUES (@bookDealerId, pAreaId);
        SET pBookDealerAreaId := LAST_INSERT_ID();
	END IF;
END