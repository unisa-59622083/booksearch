DELETE FROM areas WHERE id = 3;
ALTER TABLE areas AUTO_INCREMENT = 3;
SELECT * FROM areas;

SELECT * FROM book_dealers;

DELETE FROM books WHERE id = 3;
SELECT * FROM books;

SELECT * FROM book_dealer_areas;

SELECT * FROM book_dealer_area_books;

SELECT
	a.name as area,
    bd.name as book_dealer,
    b.title as book
FROM
	areas a
    INNER JOIN book_dealer_areas bda on a.id = bda.area_id
    INNER JOIN book_dealers bd on bda.book_dealer_id = bd.id
    INNER JOIN book_dealer_area_books bdab on bda.id = bdab.book_dealer_area_id
    INNER JOIN books b on bdab.book_id = b.id;
    
SELECT b.* FROM books b INNER JOIN book_dealer_books bdb on b.id = bdb.book_id INNER JOIN book_dealers bd on bdb.book_dealer_id = bd.id INNER JOIN areas a on bd.area_id = a.id WHERE a.id = 1;

START TRANSACTION;
CALL create_generic('Plettenberg Bay', 'Exclusive Books', 'The Name of the Wind');
ROLLBACK;

CALL delete_generic(7, 3, 5);
CALL get_summary(-1, -1, -1);