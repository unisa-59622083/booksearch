CREATE DEFINER=`root`@`localhost` PROCEDURE `create_area`(IN pAreaName VARCHAR(255), OUT pAreaId INT(11))
BEGIN
	INSERT INTO areas (`name`) VALUES (pAreaName);
	SET pAreaId := LAST_INSERT_ID();
END