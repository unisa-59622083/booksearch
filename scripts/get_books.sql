CREATE DEFINER=`root`@`localhost` PROCEDURE `get_books`()
BEGIN
	SELECT * FROM books ORDER BY title;
END