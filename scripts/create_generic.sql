CREATE DEFINER=`root`@`localhost` PROCEDURE `create_generic`(IN pAreaName VARCHAR(255), IN pBookDealerName VARCHAR(255), IN pBookTitle VARCHAR(255))
BEGIN
	SELECT @areaId := IFNULL((SELECT id FROM areas WHERE `name` = pAreaName LIMIT 1), NULL);
    
    IF ISNULL(@areaId) THEN
		CALL create_area(pAreaName, @areaId);
	END IF;
    
    
    IF (NOT ISNULL(pBookDealerName) AND pBookDealerName <> '') THEN
		SELECT @bookDealerId := IFNULL((
			SELECT bd.id
			FROM book_dealers bd
			INNER JOIN book_dealer_areas bda on bd.id = bda.book_dealer_id
			WHERE
				bda.area_id = @areaId
				AND bd.`name` = pBookDealerName
			LIMIT 1
		), NULL);
		
		IF ISNULL(@bookDealerId) THEN
			CALL create_book_dealer(pBookDealerName, @areaId, @bookDealerId, @bookDealerAreaId);
		END IF;
		
		
        IF (NOT ISNULL(pBookTitle) AND pBookTitle <> '') THEN
			SELECT @bookId := IFNULL((
				SELECT b.id
				FROM books b
				INNER JOIN book_dealer_area_books bdab ON b.id = bdab.book_id
				WHERE
					bdab.id = @bookDealerAreaId
					AND b.title = pBookTitle
				LIMIT 1
			), NULL);
			
			IF ISNULL(@bookId) THEN
				CALL create_book(pBookTitle, @bookDealerAreaId, @bookId);
			END IF;
        END IF;
    END IF;
END