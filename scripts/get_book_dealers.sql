CREATE DEFINER=`root`@`localhost` PROCEDURE `get_book_dealers`()
BEGIN
	SELECT * FROM book_dealers ORDER BY `name`;
END