CREATE DEFINER=`root`@`localhost` PROCEDURE `create_book`(IN pBookTitle VARCHAR(255), IN pBookDealerAreaId INT(11), INOUT pBookId INT(11))
BEGIN
	SELECT @bookId := IFNULL((SELECT id FROM books WHERE title = pBookTitle LIMIT 1), NULL);

	IF ISNULL(@bookId) THEN
		INSERT INTO books (title) VALUES (pBookTitle);
		SET @bookId := LAST_INSERT_ID();
    END IF;
    
    IF NOT EXISTS (SELECT * FROM book_dealer_area_books WHERE book_dealer_area_id = pBookDealerAreaId AND book_id = @bookId LIMIT 1) THEN
		INSERT INTO book_dealer_area_books (book_dealer_area_id, book_id) VALUES (pBookDealerAreaId, @bookId);
	END IF;
    
    SET pBookId := @bookId;
END