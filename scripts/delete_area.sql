CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_area`(IN pAreaId INT(11))
BEGIN
	DELETE FROM areas WHERE id = pAreaId;
END