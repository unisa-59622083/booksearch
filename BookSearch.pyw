#!python3.4

import sys
from BookSearch import *
from PyQt4.QtGui import *
from dal import DAL
from models import *


class MyForm(QtGui.QMainWindow):
    db = None
    summary = None
    selected_summary = None
    selected_row = None

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("Book Search")

        self.db = DAL()
        self.db.connect()

        self.set_areas()
        self.set_book_dealers()
        self.set_books()

        self.ui.cb_area.activated.connect(self.cb_area_changed)
        self.ui.cb_store.activated.connect(self.cb_store_changed)

        self.ui.btn_search.clicked.connect(self.btn_search_clicked)
        self.ui.btn_create.clicked.connect(self.btn_create_clicked)
        self.ui.btn_edit.clicked.connect(self.btn_edit_clicked)

        self.ui.tbl_summary.itemSelectionChanged.connect(self.tbl_summary_changed)

        self.ui.mdiArea.addSubWindow(self.ui.subwindow_read)
        self.ui.mdiArea.addSubWindow(self.ui.subwindow_create)
        self.ui.mdiArea.addSubWindow(self.ui.subwindow_edit)
        
        self.ui.mdiArea.setViewMode(1)
        self.ui.mdiArea.activateNextSubWindow()

    def set_areas(self):
        self.ui.cb_area.clear()
        self.ui.cb_area.addItem("", -1)

        for area in self.db.get_areas():
            self.ui.cb_area.addItem(area.name, area.id)

    def set_book_dealers(self):
        self.ui.cb_store.clear()
        self.ui.cb_store.addItem("", -1)

        area_id = self.ui.cb_area.itemData(self.ui.cb_area.currentIndex())
        book_dealers = None

        if (area_id == None or area_id == -1):
            book_dealers = self.db.get_book_dealers()
        else:
            book_dealers = self.db.get_book_dealers_for_area(area_id)

        for book_dealer in book_dealers:
            self.ui.cb_store.addItem(book_dealer.name, book_dealer.id)

    def set_books(self):
        self.ui.cb_book.clear()
        self.ui.cb_book.addItem("", -1)

        area_id = self.ui.cb_area.itemData(self.ui.cb_area.currentIndex())
        book_dealer_id = self.ui.cb_store.itemData(self.ui.cb_store.currentIndex())
        books = None

        if ((area_id == None or area_id == -1) and (book_dealer_id == None or book_dealer_id == -1)):
            books = self.db.get_books()
        elif area_id != None and area_id != -1 and book_dealer_id != None and book_dealer_id != -1:
            books = self.db.get_books_for_book_dealer_and_area(book_dealer_id, area_id)
        elif (area_id != None and area_id != -1):
            books = self.db.get_books_for_area(area_id)
        elif (book_dealer_id != None and book_dealer_id != -1):
            books = self.db.get_books_for_book_dealer(book_dealer_id)

        for book in books:
            self.ui.cb_book.addItem(book.title, book.id)

    def cb_area_changed(self):
        print("cb_area_changed")
        self.set_book_dealers()
        self.set_books()

    def cb_store_changed(self):
        print("cb_store_changed")
        self.set_books()

    def btn_search_clicked(self):
        print("btn_search_clicked")

        area_id = self.ui.cb_area.itemData(self.ui.cb_area.currentIndex()) or -1
        book_dealer_id = self.ui.cb_store.itemData(self.ui.cb_store.currentIndex()) or -1
        book_id = self.ui.cb_book.itemData(self.ui.cb_book.currentIndex()) or -1

        self.summary = self.db.get_summary(area_id, book_dealer_id, book_id)
        self.ui.tbl_summary.clear()
        self.ui.tbl_summary.setRowCount(len(self.summary))

        row = 0
        for summary_record in self.summary:
            self.ui.tbl_summary.setItem(row, 0, QTableWidgetItem(summary_record.area.name))
            self.ui.tbl_summary.setItem(row, 1, QTableWidgetItem(summary_record.book_dealer.name))
            self.ui.tbl_summary.setItem(row, 2, QTableWidgetItem(summary_record.book.title))
            btn = QPushButton(self.ui.tbl_summary)
            btn.setText('Delete')
            btn.clicked.connect(lambda *args, row=row: self.btn_delete_clicked(row))
            self.ui.tbl_summary.setCellWidget(row, 3, btn)
            row += 1

    def tbl_summary_changed(self):
        print("tbl_summary_changed")

        for index in self.ui.tbl_summary.selectedIndexes():
            self.selected_summary = self.summary[index.row()]
            print("area = {}, book_dealer = {}, book = {}".format(
                self.selected_summary.area.name,
                self.selected_summary.book_dealer.name,
                self.selected_summary.book.title
            ))

        self.ui.tb_edit_area.setText(self.selected_summary.area.name)
        self.ui.tb_edit_store.setText(self.selected_summary.book_dealer.name)
        self.ui.tb_edit_book.setText(self.selected_summary.book.title)

    def btn_create_clicked(self):
        print("btn_create_clicked")

        area = self.ui.tb_create_area.text()
        book_dealer = self.ui.tb_create_store.text()
        book = self.ui.tb_create_book.text()

        success = self.db.create_generic(area, book_dealer, book)

        if (success == False):
            self.ui.lbl_create.setText("Error creating new record.")
        else:
            self.ui.lbl_create.setText("Record created successfully.")
            self.set_areas()
            self.set_book_dealers()
            self.set_books()

    def btn_edit_clicked(self):
        self.selected_summary.area.name = self.ui.tb_edit_area.text()
        self.selected_summary.book_dealer.name = self.ui.tb_edit_store.text()
        self.selected_summary.book.title = self.ui.tb_edit_book.text()

        success = self.db.update_generic(self.selected_summary)

        if (success == False):
            self.ui.lbl_edit.setText("Error updating record.")
        else:
            self.ui.lbl_edit.setText("Record updated successfully.")
            self.set_areas()
            self.set_book_dealers()
            self.set_books()

    def btn_delete_clicked(self, row):
        print("btn_delete_clicked")
        
        self.db.delete_generic(self.summary[row])

        self.set_areas()
        self.set_book_dealers()
        self.set_books()

        self.ui.tbl_summary.removeRow(row)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())
