#!python3.4

import mysql.connector
from mysql.connector import errorcode
import json
from collections import OrderedDict
from models import *


class DAL:
    _db_name = None
    _connection = None
    _json_data = None

    def __init__(self):
        # Load the data.json file into an ordered dictionary.
        # Ordered dictionary is necessary, because the tables and data need to be created and inserted in a specific order.
        self._json_data = json.loads(open('data.json').read(), object_pairs_hook=OrderedDict)
        self._db_name = self._json_data['database']

    # Create the MySQL database for this program. Handles creation of table and insertion of data as well.
    def _create_database(self):
        try:
            cursor = self._connection.cursor()

            print("Creating database `{}`".format(self._db_name))

            cursor.execute("CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(self._db_name))
            self._connection.database = self._db_name

            self._create_tables(cursor)
            self._insert_data(cursor)
            self._create_procedures(cursor)

            # Persist changes made to the database.
            self._connection.commit()
            cursor.close()

        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            exit(1)

    def _create_tables(self, cursor):
        for table_name, fields in self._json_data['tables'].items():
            create_table_query = ""

            for field in fields:
                if create_table_query != "":
                    create_table_query += ", "
                create_table_query += field

            print("Creating table `{}`".format(table_name))

            cursor.execute("CREATE TABLE `{}` ({}) ENGINE=InnoDB".format(table_name, create_table_query))

    def _insert_data(self, cursor):
        for table_name, records in self._json_data['records'].items():
            print("Inserting into `{}`".format(table_name))

            for record in records:
                fields = ""
                values = ""

                for field_name, field_value in record.items():
                    if fields != "":
                        fields += ", "
                    fields += field_name

                    if values != "":
                        values += ", "
                    values += str(field_value)

                cursor.execute("INSERT INTO `{}` ({}) values ({})".format(table_name, fields, values))

    def _create_procedures(self, cursor):
        for procedure_name in self._json_data['procedures']:
            print("Creating procedure {}".format(procedure_name))
            cursor.execute(open("scripts/{}.sql".format(procedure_name)).read())

    def connect(self):
        self._connection = mysql.connector.connect(user='root', password='password')

        try:
            self._connection.database = self._db_name
        except mysql.connector.Error as err:
            # When connection fails due to the database not existing,
            # the database seed process will begin.
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                self._create_database()
            else:
                print(err)
                exit(1)
        finally:
            self._json_data = None

    def _run_query(self, query):
        cursor = self._connection.cursor()
        cursor.execute(query)
        return cursor

    # Query all `books` in the database.
    def get_books(self):
        print("get_books")

        cursor = self._connection.cursor()
        cursor.callproc("get_books")
        books = []

        for result in cursor.stored_results():
            for (ident, title) in result.fetchall():
                print("id = {}, title = {}".format(ident, title))
                books.append(Book(ident, title))

        cursor.close()
        print("")
        return books

    def get_books_for_book_dealer(self, book_dealer_id):
        print("get_books_for_book_dealer")

        cursor = self._connection.cursor()
        cursor.callproc("get_books_for_book_dealer", (book_dealer_id,))
        books = []

        for result in cursor.stored_results():
            for (ident, title) in result.fetchall():
                print("id = {}, title = {}".format(ident, title))
                books.append(Book(ident, title))

        cursor.close()
        print("")
        return books

    def get_books_for_area(self, area_id):
        print("get_books_for_book_dealer")

        cursor = self._connection.cursor()
        cursor.callproc("get_books_for_area", (area_id,))
        books = []

        for result in cursor.stored_results():
            for (ident, title) in result.fetchall():
                print("id = {}, title = {}".format(ident, title))
                books.append(Book(ident, title))

        cursor.close()
        print("")
        return books

    def get_books_for_book_dealer_and_area(self, book_dealer_id, area_id):
        print("get_books_for_book_dealer_and_area")

        cursor = self._connection.cursor()
        cursor.callproc("get_books_for_book_dealer_and_area", (book_dealer_id, area_id))
        books = []

        for result in cursor.stored_results():
            for (ident, title) in result.fetchall():
                print("id = {}, title = {}".format(ident, title))
                books.append(Book(ident, title))

        cursor.close()
        print("")
        return books

    def get_areas(self):
        print("get_areas")
        
        cursor = self._connection.cursor()
        cursor.callproc("get_areas")
        areas = []

        for result in cursor.stored_results():
            for (ident, name) in result.fetchall():
                print("id = {}, name = {}".format(ident, name))
                areas.append(Area(ident, name))

        cursor.close()
        print("")
        return areas

    def get_book_dealers(self):
        print("get_book_dealers")

        cursor = self._connection.cursor()
        cursor.callproc("get_book_dealers")
        book_dealers = []

        for result in cursor.stored_results():
            for (ident, name) in result.fetchall():
                print("id = {}, name = {}".format(ident, name))
                book_dealers.append(BookDealer(ident, name))

        cursor.close()
        print("")
        return book_dealers

    def get_book_dealers_for_area(self, area_id):
        print("get_book_dealers_for_area")

        cursor = self._connection.cursor()
        cursor.callproc("get_book_dealers_for_area", (area_id,))
        book_dealers = []

        for result in cursor.stored_results():
            for (ident, name) in result.fetchall():
                print("id = {}, name = {}".format(ident, name))
                book_dealers.append(BookDealer(ident, name))

        cursor.close()
        print("")
        return book_dealers

    def get_summary(self, area_id, book_dealer_id, book_id):
        print("get_summary")

        cursor = self._connection.cursor()
        cursor.callproc("get_summary", (area_id, book_dealer_id, book_id))
        data = []

        for result in cursor.stored_results():
            for (area_id, area_name, book_dealer_id, book_dealer_name, book_id, book_title) in result.fetchall():
                print("area_name = {}, book_dealer_name = {}, book_title = {}"
                    .format(area_name, book_dealer_name, book_title)
                )
                data.append(Summary(
                    Area(area_id, area_name),
                    BookDealer(book_dealer_id, book_dealer_name),
                    Book(book_id, book_title)
                ))

        cursor.close()
        print("")
        return data

    def create_generic(self, area, book_dealer, book):
        print("create_generic")

        success = False
        cursor = self._connection.cursor()

        try:
            cursor.callproc('create_generic', (area, book_dealer, book))
            self._connection.commit()
            success = True
        except mysql.connector.Error as err:
            print(err)
            success = False
        finally:
            cursor.close()
            print("")
            return success

    def update_generic(self, summary):
        print("update_generic")

        success = False
        cursor = self._connection.cursor()

        try:
            cursor.callproc("update_generic", (
                summary.area.id,
                summary.area.name,
                summary.book_dealer.id,
                summary.book_dealer.name,
                summary.book.id,
                summary.book.title
            ))
            self._connection.commit()
            success = True
        except mysql.connector.Error as err:
            print(err)
            success = False
        finally:
            cursor.close()
            print("")
            return success

    def delete_generic(self, summary):
        print("delete_generic")

        success = False
        cursor = self._connection.cursor()

        try:
            cursor.callproc("delete_generic", (summary.area.id, summary.book_dealer.id, summary.book.id))
            self._connection.commit()
            success = True
        except mysql.connector.Error as err:
            print(err)
            success = False
        finally:
            cursor.close()
            print("")
            return success
